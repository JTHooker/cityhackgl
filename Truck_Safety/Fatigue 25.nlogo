breed [ heavy_vehicles heavy_vehicle ]
breed [ depots depot ]
breed [ enforcers enforcer ]

heavy_vehicles-own [ hoursdriving dollars target location withoutsleep pressure crashrisk fines crash pace asleep resting timenow loselicense travelled ]
globals [ Pay_structure totalcrashed ]
depots-own [ delay offer deliveries ]
enforcers-own [ sights ]


to setup
  clear-all
        encourage_sleep
    create-depots number-of-depots [ set size 5 set color blue set offer random-poisson ( Pool / number_of_trucks )
       set shape "square" set delay random-poisson delays  setxy random-xcor random-ycor ]
      create-heavy_vehicles  number_of_trucks [ set size 10 set hoursdriving random-poisson 2 set target one-of depots
        face target set dollars 0 set withoutsleep random 16 set pressure 0 set crashrisk 1 set asleep 0 set resting 0 ]
         set enforcement 1
    create-enforcers Enforcement [ set size 10 set shape "car top" set color white setxy random-xcor random-ycor set sights one-of heavy_vehicles with [ hoursdriving > 4 ] ]
    ask depots [  setxy 0.8 * xcor 0.8 * ycor ]
    ask heavy_vehicles [ set color green set shape "truck cab top" set location one-of depots move-to location set pace 2 ]
   setup-patches
  ;; ask depots [ create-links-with other depots ]
 ;; ask links [ set color 6 ]
 reset-ticks
 ;;takemovie
  end

to setup-patches
  ask patches [ set pcolor 5 ]
  ask n-of 13467 patches [ set pcolor 6 ]
  ask n-of 13467 patches with [ pcolor = 5 ] [ set pcolor 7 ]
end


to makemoneyperload
   if Pay-Structure = "per trip" [
   set dollars [ offer ] of location rush ] ; trucks make as much money as is on offer from the consignor
   if Pay-structure = "per km" [
     set dollars [ offer ] of location rush ] ;trucks make money per km that they travel - standard hours are calibrated at 5 days per week, 8 hours per day
   if Pay-structure = "per hour" [
     set dollars ( random-poisson 1000 ) set pressure 0 ] ; trucks make money based on a standard 40 hour per week salary with mean of $25 per hour
end


to
  go
   if ticks >= 1000 [ stop ]
   if count heavy_vehicles with [ color = red ] >= 200 [ stop ]
    encourage_sleep
    policy
        ask heavy_vehicles [ make-roads checkcrash  ]
               ask heavy_vehicles [
             if distance target = 0 and Pay-structure = "per trip"

     ;per trip drivers look for the best net profit from all available depots
      [ set target one-of depots with [ offer > mean [ offer ] of depots ]
        face target set pressure 0  set hoursdriving 0 set asleep 0 set resting 0 ]

        ; per km drivers look for the longest distance without stopping from all available depots ;;set target max-one-of depots  [ distance myself ]
        if distance target = 0 and Pay-structure = "per km"
        [ set target one-of depots with [ offer > mean [ offer ] of depots ]
          face target set pressure 0 set hoursdriving 0 set asleep 0 set resting 0 ]

     ; per hour drivers look for any depot in the system
      if distance target = 0 and Pay-structure = "per hour"
          [ set target one-of depots
            face target set hoursdriving 0 set pressure 0 set asleep 0 set resting 0 ]

           ; per trip drivers look for the best net profit from all available depots and are under pressure to get there without delay
           if distance target = 0 and Pay-structure = "per trip with penalties"
          [ set target one-of depots with [ offer = max [ offer ] of depots  ]
                face target set hoursdriving 0 set pressure 0 set asleep 0 set resting 0 ]


    totalcrashes
    ;; move towards target.  once the distance is less than 1,
    ;; use move-to to land exactly on the target.
      ifelse distance target < 1
      [ move-to target set location target set travelled 0 set hoursdriving 0 ]
           [ fd pace set travelled travelled + 1 ]]
     ask depots [ howbusy receive capdepots moredepots changedelay set pcolor 6 anytruckshere ]
     ask heavy_vehicles [ moveup face target rest makemoneyperload heldup paceyourself basepressure basewithoutsleep checkenforcers rolldice tracetrucks ]
     ask enforcers [ face sights fd 1 chase moreorlessenforcers trace fd 1.5]
    tick
end

to policy
  if autopolicy [
  ; this set of policies increases police presence based on crashes in the system

  if count heavy_vehicles with [ crash = 1 ] >= 2 and count enforcers < 1000[ set enforcement enforcement + 25 ]
  if count heavy_vehicles with [ crash = 2 ] < 1 and count enforcers > 1 [ set enforcement enforcement - 1 ]
  if enforcement = 0 [ set enforcement 1 ]
   ]
end

to anytruckshere
  if any? heavy_vehicles-on patch-here [ set color pink  set deliveries deliveries + 1 ]
  if not any? heavy_vehicles-on patch-here [ set color blue ]
end

to rest
  ;Drivers only sleep if the conditions call for it - they need to be tired, away from the depot and not under a lot of pressure
     if pcolor = 7 and withoutsleep > sleepdecide and pressure <= 1
         and distance target >= random hoursaway [ set hoursdriving 0 set withoutsleep random-normal 0 2 set pace pace * .1 set asleep 1 ]
            if pcolor = 7 and loselicense = 2 and withoutsleep > sleepdecide [ set hoursdriving 0 set withoutsleep random-normal 0 2 set pace pace * .1 set asleep 1 ]
               ; Drivers only rest if the conditions call for it - they need to have driven for a few hours, away from the depot and not under too much pressure
             if pcolor = 6 and hoursdriving > restdecide and pressure <= 1
           and distance target >= hoursaway [ set hoursdriving 0 set pace pace * .5 set color green set resting 1 ]
 if pcolor = 6 and loselicense = 2 and hoursdriving > restdecide [ set hoursdriving 0 set pace pace * .5 set color green set resting 1 ]

end

to changedelay
  set delay random-normal delays .2
end

to heldup
    set pressure pressure + ([ delay ] of target)
end


to make-roads
 if pcolor = 5 [ set pace pace + 1 set pace 2 set hoursdriving hoursdriving + 1 set withoutsleep withoutsleep + 1 ]
end

to rush
   if pace < mean [ pace ] of heavy_vehicles [ set pressure pressure + 1 ]
   if pace > mean [ pace ] of heavy_vehicles [ set pressure pressure - 1 ]
end

to receive
   if Pay-Structure = "per trip" and remainder ( ticks ) ( 50 ) = 0 [ set offer random-poisson 1000 ]
   if Pay-Structure = "per km" and remainder ( ticks ) ( 50 ) = 0 [ set offer mean [ distance myself] of heavy_vehicles * ( random-poisson per_km_rate ) ]
end

to howbusy
   set size  5 + count heavy_vehicles-on patch-here
end

to capdepots
  if count depots > number-of-depots [ die ]
end

to moredepots
  if count depots < number-of-depots [ hatch 1 fd 5 ]
end

to encourage_sleep
  ask patches [
          if random-float 1000 < more_or_less_sleep
        [ set pcolor 7 ]
        if random-float 1000 < rest-breaks
        [ set pcolor 6 ]
        if random-float 1000 < Economic_Pressure
        [set pcolor 5 ]
   ]
end

to moveup
      if dollars < mean [ dollars ] of heavy_vehicles [ set pressure pressure + 1 ]
      if dollars > mean [ dollars ] of heavy_vehicles [ set pressure pressure - 1 ]
  end

to paceyourself
 if pace > Speed-limit
 [ set pace speed-limit ]
 if pace < speed-min
 [ set pace speed-min ]
end

to basepressure
  if pressure <= 0 [ set pressure 0 ]
  if pressure >= 10 [ set pressure 10 ]
end

to basewithoutsleep
  if withoutsleep >= 24 [ set withoutsleep 24 ]
end

to checkcrash
  if hoursdriving > 4 [ set crashrisk 2 set color red ]
  if hoursdriving <= 4 [ set crashrisk 1 set color green ]
end

to checkenforcers
  if any? enforcers-on patch-here and hoursdriving > 4 [ set hoursdriving 0 set fines fines + 1 set dollars dollars * .8 set pace pace * .5 set timenow ticks ]
  if any? enforcers-on patch-here and withoutsleep > 16 [ set withoutsleep 0 set fines fines + 1 set dollars dollars * .8 set pace pace * .5 set timenow ticks ]
  if ticks - timenow > 100 [ set fines fines - 1 ]
  if fines >= 3 [ set loselicense loselicense + 1 set fines 0 ]
  if fines <= 0 [ set fines 0 ]
end


to chase
  if [ hoursdriving ] of sights  < 4 and [ withoutsleep ] of sights < 16 [ set sights one-of heavy_vehicles with [ hoursdriving > 4 ]  ]
end

to moreorlessenforcers
  if count Enforcers < Enforcement [ hatch 1 face sights fd random 10 ]
  if count Enforcers > Enforcement  [ die ]
end

to rolldice
  ifelse crashrisk > random 1000 [ set crash 1 set color yellow set shape "star" set size 20 ]
  [ set crash 0 set shape "truck cab top" set size 10 ]
end

to trace
  if pen_down [ pen-down ]
  if not pen_down [ penup ]
end


to tracetrucks
  if PD_trucks [ pen-down ]
  if not PD_trucks [ penup ]
end

to totalcrashes
  set totalcrashed totalcrashed + count ( heavy_vehicles with [ shape = "star" ]) / 200
end
@#$#@#$#@
GRAPHICS-WINDOW
901
4
1673
777
-1
-1
3.801
1
10
1
1
1
0
0
0
1
-100
100
-100
100
1
1
1
ticks
30.0

BUTTON
11
15
74
48
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
109
15
172
48
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

MONITOR
13
54
70
99
Trucks
count heavy_vehicles
17
1
11

PLOT
290
273
678
410
Hours and pressure
NIL
NIL
0.0
10.0
0.0
16.0
true
true
"" ""
PENS
"Fatigue" 1.0 0 -2674135 true "" "plot mean [ hoursdriving ] of heavy_vehicles"
"NoSleep" 1.0 0 -7500403 true "" "plot mean [ withoutsleep ] of heavy_vehicles"
"SD sleep" 1.0 0 -955883 true "" "plot standard-deviation [ withoutsleep ] of heavy_vehicles"

PLOT
3
273
277
409
Speed and Pressure
NIL
NIL
0.0
10.0
0.0
100.0
true
true
"" ""
PENS
"AvSpeed" 1.0 0 -16777216 true "" "plot mean [ pace ] of heavy_vehicles * 50"
"Pressure" 1.0 0 -955883 true "" "plot mean [ pressure ] of heavy_vehicles * 10"
"Distance" 1.0 0 -7500403 true "" "plot mean [ travelled ] of heavy_vehicles"

MONITOR
13
160
78
205
% danger
100 * (count heavy_vehicles with [ hoursdriving > 4 ])/ (count heavy_vehicles )
2
1
11

MONITOR
93
161
161
206
Sleephrs
24 - mean [ withoutsleep ] of heavy_vehicles
2
1
11

SLIDER
4
416
176
449
speed-limit
speed-limit
0
10
10.0
.01
1
NIL
HORIZONTAL

SLIDER
445
421
617
454
hoursaway
hoursaway
0
10
2.0
.25
1
NIL
HORIZONTAL

SLIDER
389
16
561
49
number-of-depots
number-of-depots
0
100
50.0
1
1
NIL
HORIZONTAL

SLIDER
389
53
561
86
number_of_trucks
number_of_trucks
0
200
200.0
1
1
NIL
HORIZONTAL

PLOT
385
143
585
263
Mean Income
NIL
NIL
700.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot mean [ dollars ] of heavy_vehicles"

CHOOSER
12
109
185
154
Pay-Structure
Pay-Structure
"per trip" "per hour" "per km"
0

MONITOR
15
214
162
259
Mean Offer
mean [ offer ] of depots
0
1
11

SLIDER
203
56
375
89
more_or_less_sleep
more_or_less_sleep
1
100
15.0
1
1
NIL
HORIZONTAL

MONITOR
201
420
328
465
Mean hours without rest
mean [ hoursdriving ] of heavy_vehicles
2
1
11

SLIDER
202
129
377
162
Per_km_rate
Per_km_rate
0
20
11.5
.01
1
$
HORIZONTAL

SLIDER
202
14
374
47
Per_hour_rate
Per_hour_rate
0
200
20.0
1
1
$
HORIZONTAL

SLIDER
203
92
375
125
Rest-breaks
Rest-breaks
1
100
15.0
1
1
NIL
HORIZONTAL

SLIDER
4
452
176
485
Speed-min
Speed-min
0
10
0.0
.01
1
NIL
HORIZONTAL

SLIDER
445
458
617
491
sleepdecide
sleepdecide
0
20
14.0
1
1
NIL
HORIZONTAL

SLIDER
201
468
373
501
Restdecide
Restdecide
0
10
3.0
1
1
NIL
HORIZONTAL

SLIDER
391
93
563
126
Economic_Pressure
Economic_Pressure
0
100
15.0
1
1
NIL
HORIZONTAL

MONITOR
598
217
662
262
Av Speed
mean [ pace ] of heavy_vehicles * 50
1
1
11

SLIDER
6
487
178
520
Delays
Delays
-1
1
0.1
.1
1
NIL
HORIZONTAL

MONITOR
588
82
648
127
Pressure
mean [ pressure ] of heavy_vehicles
1
1
11

SWITCH
572
16
684
49
autopolicy
autopolicy
0
1
-1000

MONITOR
339
421
396
466
Risky?
count heavy_vehicles with [ crashrisk = 1 ] / count heavy_vehicles * 100
0
1
11

PLOT
19
536
264
686
Fatigued Drivers and Police
NIL
NIL
0.0
10.0
0.0
200.0
true
true
"" ""
PENS
"Fatigued" 1.0 0 -16777216 true "" "plot count heavy_vehicles with [ color = red ]"
"Police" 1.0 0 -2674135 true "" "plot count enforcers"

PLOT
279
536
444
686
SD fatigue
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot standard-deviation [ hoursdriving ] of heavy_vehicles"

SLIDER
445
497
617
530
Enforcement
Enforcement
1
1000
25.0
1
1
NIL
HORIZONTAL

MONITOR
451
537
544
590
Lost License
sum [ loselicense ] of heavy_vehicles
17
1
13

MONITOR
548
537
615
590
Crashed
count heavy_vehicles with [ crash = 1 ]
17
1
13

MONITOR
452
592
509
645
Cops
count enforcers
17
1
13

SWITCH
590
135
705
168
Pen_down
Pen_down
1
1
-1000

SWITCH
591
173
705
206
PD_Trucks
PD_Trucks
1
1
-1000

MONITOR
620
538
677
591
Fines
mean [ fines ] of heavy_vehicles
17
1
13

MONITOR
453
647
527
700
Deliveries
sum [ deliveries ] of depots
0
1
13

MONITOR
512
592
600
645
Total crashed
totalcrashed
0
1
13

MONITOR
606
594
675
647
Distance
mean [ travelled ] of heavy_vehicles
0
1
13

MONITOR
547
651
604
704
LL?
count heavy_vehicles with [ fines = 2 ]
0
1
13

SLIDER
203
167
375
200
Pool
Pool
0
100000
100000.0
1
1
NIL
HORIZONTAL

MONITOR
171
217
228
258
Drive
count patches with [ pcolor = 5 ]
0
1
10

MONITOR
235
218
293
259
Rest
count patches with [ pcolor = 6 ]
0
1
10

MONITOR
301
219
358
260
Sleep
count patches with [ pcolor = 7 ]
17
1
10

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

ambulance
false
0
Rectangle -7500403 true true 30 90 210 195
Polygon -7500403 true true 296 190 296 150 259 134 244 104 210 105 210 190
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Circle -16777216 true false 69 174 42
Rectangle -1 true false 288 158 297 173
Rectangle -1184463 true false 289 180 298 172
Rectangle -2674135 true false 29 151 298 158
Line -16777216 false 210 90 210 195
Rectangle -16777216 true false 83 116 128 133
Rectangle -16777216 true false 153 111 176 134
Line -7500403 true 165 105 165 135
Rectangle -7500403 true true 14 186 33 195
Line -13345367 false 45 135 75 120
Line -13345367 false 75 135 45 120
Line -13345367 false 60 112 60 142

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

bike
false
1
Line -7500403 false 163 183 228 184
Circle -7500403 false false 213 184 22
Circle -7500403 false false 156 187 16
Circle -16777216 false false 28 148 95
Circle -16777216 false false 24 144 102
Circle -16777216 false false 174 144 102
Circle -16777216 false false 177 148 95
Polygon -2674135 true true 75 195 90 90 98 92 97 107 192 122 207 83 215 85 202 123 211 133 225 195 165 195 164 188 214 188 202 133 94 116 82 195
Polygon -2674135 true true 208 83 164 193 171 196 217 85
Polygon -2674135 true true 165 188 91 120 90 131 164 196
Line -7500403 false 159 173 170 219
Line -7500403 false 155 172 166 172
Line -7500403 false 166 219 177 219
Polygon -16777216 true false 187 92 198 92 208 97 217 100 231 93 231 84 216 82 201 83 184 85
Polygon -7500403 true true 71 86 98 93 101 85 74 81
Rectangle -16777216 true false 75 75 75 90
Polygon -16777216 true false 70 87 70 72 78 71 78 89
Circle -7500403 false false 153 184 22
Line -7500403 false 159 206 228 205

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

box 2
false
0
Polygon -7500403 true true 150 285 270 225 270 90 150 150
Polygon -13791810 true false 150 150 30 90 150 30 270 90
Polygon -13345367 true false 30 90 30 225 150 285 150 150

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

car top
true
0
Polygon -7500403 true true 151 8 119 10 98 25 86 48 82 225 90 270 105 289 150 294 195 291 210 270 219 225 214 47 201 24 181 11
Polygon -16777216 true false 210 195 195 210 195 135 210 105
Polygon -16777216 true false 105 255 120 270 180 270 195 255 195 225 105 225
Polygon -16777216 true false 90 195 105 210 105 135 90 105
Polygon -1 true false 205 29 180 30 181 11
Line -7500403 false 210 165 195 165
Line -7500403 false 90 165 105 165
Polygon -16777216 true false 121 135 180 134 204 97 182 89 153 85 120 89 98 97
Line -16777216 false 210 90 195 30
Line -16777216 false 90 90 105 30
Polygon -1 true false 95 29 120 30 119 11

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

clock
true
0
Circle -7500403 true true 30 30 240
Polygon -16777216 true false 150 31 128 75 143 75 143 150 158 150 158 75 173 75
Circle -16777216 true false 135 135 30

cloud
false
0
Circle -7500403 true true 13 118 94
Circle -7500403 true true 86 101 127
Circle -7500403 true true 51 51 108
Circle -7500403 true true 118 43 95
Circle -7500403 true true 158 68 134

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dollar bill
false
0
Rectangle -7500403 true true 15 90 285 210
Rectangle -1 true false 30 105 270 195
Circle -7500403 true true 120 120 60
Circle -7500403 true true 120 135 60
Circle -7500403 true true 254 178 26
Circle -7500403 true true 248 98 26
Circle -7500403 true true 18 97 36
Circle -7500403 true true 21 178 26
Circle -7500403 true true 66 135 28
Circle -1 true false 72 141 16
Circle -7500403 true true 201 138 32
Circle -1 true false 209 146 16
Rectangle -16777216 true false 64 112 86 118
Rectangle -16777216 true false 90 112 124 118
Rectangle -16777216 true false 128 112 188 118
Rectangle -16777216 true false 191 112 237 118
Rectangle -1 true false 106 199 128 205
Rectangle -1 true false 90 96 209 98
Rectangle -7500403 true true 60 168 103 176
Rectangle -7500403 true true 199 127 230 133
Line -7500403 true 59 184 104 184
Line -7500403 true 241 189 196 189
Line -7500403 true 59 189 104 189
Line -16777216 false 116 124 71 124
Polygon -1 true false 127 179 142 167 142 160 130 150 126 148 142 132 158 132 173 152 167 156 164 167 174 176 161 193 135 192
Rectangle -1 true false 134 199 184 205

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

food
false
0
Polygon -7500403 true true 30 105 45 255 105 255 120 105
Rectangle -7500403 true true 15 90 135 105
Polygon -7500403 true true 75 90 105 15 120 15 90 90
Polygon -7500403 true true 135 225 150 240 195 255 225 255 270 240 285 225 150 225
Polygon -7500403 true true 135 180 150 165 195 150 225 150 270 165 285 180 150 180
Rectangle -7500403 true true 135 195 285 210

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

house ranch
false
0
Rectangle -7500403 true true 270 120 285 255
Rectangle -7500403 true true 15 180 270 255
Polygon -7500403 true true 0 180 300 180 240 135 60 135 0 180
Rectangle -16777216 true false 120 195 180 255
Line -7500403 true 150 195 150 255
Rectangle -16777216 true false 45 195 105 240
Rectangle -16777216 true false 195 195 255 240
Line -7500403 true 75 195 75 240
Line -7500403 true 225 195 225 240
Line -16777216 false 270 180 270 255
Line -16777216 false 0 180 300 180

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

train passenger car
false
0
Polygon -7500403 true true 15 206 15 150 15 135 30 120 270 120 285 135 285 150 285 206 270 210 30 210
Circle -16777216 true false 240 195 30
Circle -16777216 true false 210 195 30
Circle -16777216 true false 60 195 30
Circle -16777216 true false 30 195 30
Rectangle -16777216 true false 30 140 268 165
Line -7500403 true 60 135 60 165
Line -7500403 true 60 135 60 165
Line -7500403 true 90 135 90 165
Line -7500403 true 120 135 120 165
Line -7500403 true 150 135 150 165
Line -7500403 true 180 135 180 165
Line -7500403 true 210 135 210 165
Line -7500403 true 240 135 240 165
Rectangle -16777216 true false 5 195 19 207
Rectangle -16777216 true false 281 195 295 207
Rectangle -13345367 true false 15 165 285 173
Rectangle -2674135 true false 15 180 285 188

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

truck cab top
true
0
Rectangle -7500403 true true 70 45 227 120
Polygon -7500403 true true 150 8 118 10 96 17 90 30 75 135 75 195 90 210 150 210 210 210 225 195 225 135 209 30 201 17 179 10
Polygon -16777216 true false 94 135 118 119 184 119 204 134 193 141 110 141
Line -16777216 false 130 14 168 14
Line -16777216 false 130 18 168 18
Line -16777216 false 130 11 168 11
Line -16777216 false 185 29 194 112
Line -16777216 false 115 29 106 112
Line -16777216 false 195 225 210 240
Line -16777216 false 105 225 90 240
Polygon -16777216 true false 210 195 195 195 195 150 210 143
Polygon -16777216 false false 90 143 90 195 105 195 105 150 90 143
Polygon -16777216 true false 90 195 105 195 105 150 90 143
Line -7500403 true 210 180 195 180
Line -7500403 true 90 180 105 180
Line -16777216 false 212 44 213 124
Line -16777216 false 88 44 87 124
Line -16777216 false 223 130 193 112
Rectangle -7500403 true true 225 133 244 139
Rectangle -7500403 true true 56 133 75 139
Rectangle -7500403 true true 120 210 180 240
Rectangle -7500403 true true 93 238 210 270
Rectangle -16777216 true false 200 217 224 278
Rectangle -16777216 true false 76 217 100 278
Circle -16777216 false false 135 240 30
Line -16777216 false 77 130 107 112
Rectangle -16777216 false false 107 149 192 210
Rectangle -1 true false 180 9 203 17
Rectangle -1 true false 97 9 120 17

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0
@#$#@#$#@
@#$#@#$#@
1.0
    org.nlogo.sdm.gui.AggregateDrawing 1
        org.nlogo.sdm.gui.StockFigure "attributes" "attributes" 1 "FillColor" "Color" 225 225 182 142 129 60 40
            org.nlogo.sdm.gui.WrappedStock "" "" 0
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="3" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="2000"/>
    <metric>count turtles</metric>
    <metric>count patches with [ pcolor = red ]</metric>
    <metric>count patches with [ pcolor = orange ]</metric>
    <metric>count patches with [ pcolor = green ]</metric>
    <metric>count cars with [ collisionsbikes = 1 ]</metric>
    <metric>count pedestrians with [ collisions = 1]</metric>
    <metric>count bicycles with [ collisionsbikes = 1 ]</metric>
    <metric>count pedestrians</metric>
    <metric>count cars</metric>
    <metric>count bicycles</metric>
    <metric>count patches with [ pcolor = grey ]</metric>
    <metric>mean [ VRUdensity ] of bicycles</metric>
    <enumeratedValueSet variable="gdp-increase">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="truck-on-truck">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="car-on-pedestrian">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="truck-on-pedestrian">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Initial_cars">
      <value value="5000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Max_Speed_Cars">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="car-on-car">
      <value value="-2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="energy-efficiency">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial_trains">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="energy-from-roads">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Initial_pedestrians">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Initial_heavy_vehicles">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population_growth">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Initial_bicycles">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Density">
      <value value="350"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fuel_increase">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Max_Speed_Bikes">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Dispersion">
      <value value="50"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Fatigue" repetitions="100" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1000"/>
    <metric>mean [ fines ] of heavy_vehicles</metric>
    <metric>count enforcers</metric>
    <metric>mean [ withoutsleep ] of heavy_vehicles</metric>
    <metric>mean [ hoursdriving ] of heavy_vehicles</metric>
    <metric>mean [ pace ] of heavy_vehicles</metric>
    <metric>count heavy_vehicles with [ hoursdriving &gt; 4 ]</metric>
    <metric>standard-deviation [ hoursdriving ] of heavy_vehicles</metric>
    <metric>standard-deviation [ withoutsleep ] of heavy_vehicles</metric>
    <metric>mean [ travelled ] of heavy_vehicles</metric>
    <metric>count heavy_vehicles with [ crash = 1 ]</metric>
    <metric>sum [ loselicense ] of heavy_vehicles</metric>
    <metric>totalcrashed</metric>
    <metric>mean [ pressure ] of heavy_vehicles</metric>
    <metric>standard-deviation [ pressure ] of heavy_vehicles</metric>
    <metric>sum [ deliveries ] of depots</metric>
    <metric>mean [ dollars ] of heavy_vehicles</metric>
    <metric>standard-deviation [ dollars ] of heavy_vehicles</metric>
    <enumeratedValueSet variable="Pay-Structure">
      <value value="&quot;per hour&quot;"/>
      <value value="&quot;per km&quot;"/>
      <value value="&quot;per trip&quot;"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
