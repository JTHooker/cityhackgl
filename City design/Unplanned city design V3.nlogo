globals [
  percent-similar  ;; on the average, what percent of a turtle's neighbors
                   ;; are the same color as that turtle?
  percent-unhappy  ;; what percent of the turtles are unhappy?
  density ;; the number of residents allowable on each patch
  spread ;; the spread of essential services across the city
  land ;; all patches
  ]

breed [ residents resident ] ;; people living in the city
breed [ workplaces workplace ];; workplaces in the city
breed [ CCs CC ];; city centres

residents-own [
  happy?       ;; for each turtle, indicates whether at least %-similar-wanted percent of
               ;; that turtles' neighbors are the same color as the turtle
  toofar?       ;; too far away from desired resources
  similar-nearby   ;; how available are the people and resources I want / need?
  other-nearby ;; how many have a resident of another color?
  total-nearby  ;; sum of previous two variables
  UT ;; Overall utility function for resident
  HHsize ;; how many people in the family
  HHIncome ;; Overall household income
  Targetemployer ;; Residents' employer of choice
  Targetschool ;; Residents'school of choice
  Empcommute ;; Distance to work
  Schoolcommute ;; Distance to school
  Servicecommute ;; Distance to general services
  Demographic ;; Racial / Demographic group ( 1 of 3 )
  DesiredCommute ;; desired commuting distance
  Targetbeach ;; desired beach
  Landvalue  ;; Value of land on patch here
  ]

patches-own [
  topography residentcount traffic path closeness
]

to setup
    ;;reset-ticks
    clear-all

ask patches  [ set pcolor 99.9 - (sqrt topography) ]

repeat 10 [ diffuse topography .999  ]

set attract true

if number > count patches
    [ user-message (word "This world only has room for " count patches " residents.")
      stop ]

 ask n-of Employers patches
  [ sprout-workplaces 1
    [ set color black set shape "dot" set xcor random-normal 0  (employment-density) set ycor random-normal 0 (employment-density) set size 5 ]   ]

   ask n-of number patches
    [ sprout-residents 1
      [ set size 1 set HHincome random 3 set shape "square" set desiredcommute commute set targetemployer one-of workplaces set xcor random-normal 0 20 set ycor random-normal 0 20 set closeness (distancexy 0 0)  updatecolor ]
  ]

   ;; set up city centre

   update-variables
ask patches [ set pcolor 99.9 - (sqrt topography) ]
     reset-ticks
end

to go
 ;; if all? residents [happy?] [ stop ]
 ;; growcity
 ;;if ticks > 1501 [ stop ]
  if Attract = True [
  update-variables
  adjustemployers
  adjustnumbers

    ask residents [
      update-turtles
      lookaround
      update-turtles
      roadspace
      toomany
      move-unhappy-turtles
      if Buying_Power = true [ buyingpower ]
         ]
  ]
ask patches  [  countresidents ]

 tick
end

to countresidents
  set residentcount count residents-here
end

to desiredlocation
 if attract = true [ lookaround ]
end

to toomany
   if count residents-here > crowding [ find-new-spot ]
end

to update-variables
 ask residents [  update-turtles ]
  update-globals
end

to lookaround
     ;; set desiredcommute commute
       if distance targetemployer > desiredcommute
          [ set toofar? 1 face targetemployer fd .1]
       if distance targetemployer < desiredcommute
          [ set toofar? 0 ]
end


to update-turtles
    ;; in next two lines, we use "in-radius" to test patches
    ;; surrounding the current patch
    set similar-nearby count ( residents-on neighbors )
      with [ HHincome >= [ HHincome ] of myself]
    set other-nearby count ( residents-on neighbors )
      with [ HHincome < [ HHincome ] of myself]
    set total-nearby similar-nearby + other-nearby
    set happy? similar-nearby >= ( %-similar-wanted * total-nearby / 100 )

end

to move-unhappy-turtles
  ask residents with [ happy? = false ] [ moveaway ]
 ;; ask residents with [ happy? = true ] [ move-to patch-here ]
end

to roadspace
   if count residents-on neighbors < NeighbourMin  or count residents-on neighbors > NeighbourMax [ find-new-spot ]
end

to movecloser
  face targetemployer fd .1
end

to update-globals
  let similar-neighbors sum [ similar-nearby ] of residents
  let total-neighbors sum [total-nearby] of residents
  set percent-similar (similar-neighbors / total-neighbors) * 100
  set percent-unhappy (count residents with [ not happy?] ) / (count residents) * 100
end

to adjustemployers
  ask workplaces [
        if count workplaces < employers [ hatch 1 rt random 360 fd random sqrt 10 ]
  ]
  end

to find-new-spot
        face targetemployer lt random 360 set desiredcommute desiredcommute + .1 fd random-normal 1 .01 ;;move-to patch-here
    ;; keep going until we find an unoccupied patch
  if desiredcommute > 15 and remainder ticks 50 = 0 [ set desiredcommute commute ]
end

to adjustnumbers
    if count residents < number [ ask n-of 1 patches [ sprout-residents 10 [ set size 1 set HHincome random 3 set shape "square" set desiredcommute commute updatecolor
      set targetemployer one-of workplaces ]]]
  if count residents > number [ ask n-of 10 residents [ die ]  ]
end

to updatecolor
  if HHIncome = 0 [ set color red ]
  if HHIncome = 1 [ set color yellow ]
  if HHIncome = 2 [ set color green ]
end

to moveaway
  set heading heading + 5 - 5 fd .01
end

to buyingpower
  if HHincome = 0 and any? residents-here with [ HHIncome > 0 ]
  [ moveaway ]
  if HHincome = 1 and any? residents-here with [ HHIncome > 1 ]
  [ moveaway ]
end
@#$#@#$#@
GRAPHICS-WINDOW
523
47
1330
855
-1
-1
11.7705
1
10
1
1
1
0
1
1
1
-30
30
-30
30
0
0
1
ticks
30.0

MONITOR
23
706
99
751
% unhappy
percent-unhappy
1
1
11

MONITOR
22
544
97
589
% similar
percent-similar
1
1
11

SLIDER
19
22
231
55
number
number
50
3000
1180.0
10
1
NIL
HORIZONTAL

SLIDER
19
95
231
128
%-similar-wanted
%-similar-wanted
0
100
0.0
10
1
%
HORIZONTAL

BUTTON
48
58
128
91
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
129
58
209
91
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
17
300
196
333
employment-density
employment-density
0
20
11.0
1
1
NIL
HORIZONTAL

SLIDER
18
341
191
374
Employers
Employers
0
50
3.0
1
1
NIL
HORIZONTAL

SLIDER
21
381
194
414
Commute
Commute
0
50
0.0
1
1
NIL
HORIZONTAL

MONITOR
20
595
109
648
Workplaces
count workplaces
0
1
13

SWITCH
20
424
123
457
Grow
Grow
0
1
-1000

SWITCH
20
463
123
496
Attract
Attract
0
1
-1000

PLOT
17
141
217
291
Percenthappy
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count residents with [ happy? = true ] /  count residents * 100"

CHOOSER
1396
36
1534
81
AttractionType
AttractionType
"Resident" "School" "Workplace" "OpenSpace"
2

CHOOSER
1399
97
1537
142
ChartType
ChartType
"targetemployer" "targetSchool" "targetBeach"
0

BUTTON
1400
149
1475
182
Refresh
ask residents [ set targetemployer one-of workplaces ]
NIL
1
T
TURTLE
NIL
NIL
NIL
NIL
1

MONITOR
20
656
118
701
Total Residents
count residents
0
1
11

SLIDER
330
339
503
373
NeighbourMin
NeighbourMin
0
20
5.0
1
1
NIL
HORIZONTAL

SLIDER
332
380
504
413
NeighbourMax
NeighbourMax
0
40
15.0
1
1
NIL
HORIZONTAL

SLIDER
332
427
504
460
Crowding
Crowding
0
50
30.0
1
1
NIL
HORIZONTAL

BUTTON
1400
187
1465
220
All Die
ask turtles [ die ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
333
530
452
564
Colour Roads
ask patches [ if any? residents-here [ set pcolor black ]\nif not any? residents-here [ set pcolor white ]  ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
332
468
460
501
Refresh commute
ask residents [ set desiredcommute Commute ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
1376
278
2013
757
Average Commute
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot mean [ desiredcommute ] of residents"
"Red" 1.0 0 -5298144 true "" "plot mean [ desiredcommute ] of residents with [ color = red ] "
"Green" 1.0 0 -13840069 true "" "plot mean [ desiredcommute ] of residents with [ color = green ] "
"Yellow" 1.0 0 -7171555 true "" "plot mean [ desiredcommute ] of residents with [ color = yellow ] "

SWITCH
344
95
477
129
Buying_Power
Buying_Power
0
1
-1000

BUTTON
333
567
452
601
Return to white
ask patches [ set pcolor white ] 
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## WHAT IS IT?

This project models the behaviour of people in a growing city. Each person has a level of Household Income (red - Low, orange - Middle, and green - High)

People search for a location to live that is closer than their desired commuting distance to schools and their workplace - controlled by the 'commute' slider.

The value of land that people occupy increases as demand for the land increases. If one person wants to live there, the land is low value. If two want to live there, the land is med value, and if three want to live there, it becomes high value. If the value extends beyond 2 income levels of the Household, they become 'priced out' of the market and decide to move.

People also prefer to associate themselves with people who are at their level of income or above. If the proportion of people's surrounding neighbours who are below their income level exceeds the '%_similar_wanted' slider, they move to find a more solubrious neighbourhood where they can be with their people.

Hilly areas where no-one can live are randomly generated in the environment.

Making changes to any of these preferences alters the shape and structure of the city. Typically, a higher density city where people prefere to have at least 50% of their neighbours of the same HHIncome class produces a high-price inner city area, surrounded by middle income suburbs and low income, low density outer suburbs, which are at long distances from work and school services.

Monitors keep track of the mean distance of each HHIncome class to work and school options.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

box 2
false
0
Polygon -7500403 true true 150 285 270 225 270 90 150 150
Polygon -13791810 true false 150 150 30 90 150 30 270 90
Polygon -13345367 true false 30 90 30 225 150 285 150 150

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.3
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="Popdensity">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="School-density">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Grow">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="TooSteep">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Commute">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="employment-density">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Trailer">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-similar-wanted">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Employers">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Schools">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Hilly">
      <value value="17"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
